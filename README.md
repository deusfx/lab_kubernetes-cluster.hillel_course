Notes for me
1. swap disable
2. check mac address and check the product_uuid "sudo cat /sys/class/dmi/id/product_uuid". some virtual machines may have identical values. for example: template vm VMWare
3. check ports
4. install runtime(docker)
5. install kubelet, kubeadm, kubectl
6. kubeadm init with pod network
7. deploy a Container Network Interface (CNI) (Calico or other)
8. kubectl get nodes, all nodes may be "ready"
9. kubectl apply -f ...yml


```
roothosts@ubnt-04Andrey:~/dashboard-kuber$ kubectl get nodes -n lab15-dashboard
NAME            STATUS   ROLES                  AGE   VERSION
ubnt-04andrey   Ready    control-plane,master   17h   v1.20.4
ubnt-05-node    Ready    <none>                 17h   v1.20.4
```


```
roothosts@ubnt-04Andrey:~/dashboard-kuber$ kubectl get pods -n lab15-dashboard
NAME                         READY   STATUS    RESTARTS   AGE
db-lab15-f856b7965-6mwvs     1/1     Running   0          16h
db-lab15-f856b7965-6s5fx     1/1     Running   0          16h
web-lab15-754bb95867-pcn84   1/1     Running   0          17h
web-lab15-754bb95867-tdg2f   1/1     Running   0          17h
```

```
roothosts@ubnt-04Andrey:~/dashboard-kuber$ kubectl get deployments -n lab15-dashboard
NAME        READY   UP-TO-DATE   AVAILABLE   AGE
db-lab15    2/2     2            2           16h
web-lab15   2/2     2            2           17h
```

