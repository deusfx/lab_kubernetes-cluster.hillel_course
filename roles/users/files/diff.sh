#!/bin/bash
#$1 - hostuser
for f in /tmp/mykeys/*.pub
do
  A="$f"
  B=/home/"$1"/.ssh/id_rsa.pub
  DIFF=$(diff "$A" "$B")
  if [ "$DIFF" != "" ]
  then
      cat "$f" > /home/"$1"/.ssh/authorized_keys
  fi
done

